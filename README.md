<img src="img/banner.jpeg">
# GitLab Essential Training

## **Software Required**

1. Git : [Download](https://git-scm.com/downloads)
2. ATOM Editor : [Download](https://atom.io/)

## **Content Covers**

- Introduction to GitLab.
- Uses and Features of GitLab.
- Software Installation and Account Creation.
- Creation of Group, Repository and Projects.
- Basic Commands of GitLab.
- Creation of Markdown File using ATOM Text Editor.
- Push the first Project.
- Create issues and close issues.
- Hands On Practise of the Commands.


## **Coordinator**
  > ####  Prof. Navjyotsinh Jadeja

## **Student Coordinator**

> 1. Kunal Dholiya
> 2. Riddhi Sherasiya
> 3. Vibha Sanghani
